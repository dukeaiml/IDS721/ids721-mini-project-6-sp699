# IDS-721-Cloud-Computing :computer:

## Mini Project 6 :page_facing_up: 

## :ballot_box_with_check: Requirements
* Add logging to a Rust Lambda function
* Integrate AWS X-Ray tracing
* Connect logs/traces to CloudWatch

## :ballot_box_with_check: Grading Criteria
* __Logging implementation__ 30%
* __X-Ray tracing__ 30%
* __CloudWatch centralization__ 30%
* __Documentation__ 10%

## :ballot_box_with_check: Deliverables
* Rust code
* Screenshots or demo video showing successful invocation
* Writeup explaining service

## :ballot_box_with_check: Main Progress
1. __`Create the Rust function with Logging`__: Create a new directory with Cargo Lambda and create the Rust function. In this project, the Rust function computes the avaerage value from the input numbers list. 
```rust
use lambda_runtime::{Error, LambdaEvent, service_fn};
use log::{info, warn};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use simple_logger::SimpleLogger;

#[derive(Deserialize)]
struct CustomEvent {
    numbers: Vec<i32>,
}

#[derive(Serialize)]
struct CustomOutput {
    average: Option<f64>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    SimpleLogger::new().init().unwrap();
    let func = service_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn func(event: LambdaEvent<Value>) -> Result<CustomOutput, Error> {
    let (event, _context) = event.into_parts();
    // Deserialize the event to our CustomEvent struct
    let event: CustomEvent = serde_json::from_value(event)?;

    match calculate_average(&event.numbers) {
        Some(average) => {
            info!("The average is: {}", average);
            Ok(CustomOutput { average: Some(average) })
        },
        None => {
            warn!("Could not calculate the average. The input list was empty.");
            Ok(CustomOutput { average: None })
        },
    }
}

/// Calculates the average of a list of numbers.
/// Logs the start and end of the computation, and warns if the list is empty.
fn calculate_average(numbers: &[i32]) -> Option<f64> {
    if numbers.is_empty() {
        warn!("Attempted to calculate the average of an empty list.");
        return None;
    }

    info!("Starting calculation for the average.");
    let sum: i32 = numbers.iter().sum();
    let count = numbers.len() as f64;
    let average = sum as f64 / count;

    info!("Finished calculation for the average.");
    Some(average)
}
```
- In this project, we added a logging function to the Rust function.
```rust
use log::{info, warn};
use simple_logger::SimpleLogger;
```
- Dependencies in Cargo.toml file
```rust
[dependencies]
log = "0.4"
simple_logger = "2.0"
```

2. __`Build and Deploy the Rust Function to AWS Lambda`__: After creating Rust function in `main.rs`, build and deploy the function to AWS.
```bash
$ cargo lambda build --release
$ cargo lambda deploy
```

3. __`Integrate AWS X-Ray tracing`__: Integreate AWS X-ray tracing with AWS Lambda function.
(1) Conduct a test to see if the function works properly.
- Test Example
![Test Example](https://github.com/suim-park/Cloud-Mini-Project-1/assets/143478016/dcd77e6e-9906-45c8-89b3-7351a7346135)
- Test Result
![Test Result](https://github.com/suim-park/Cloud-Mini-Project-1/assets/143478016/38677be7-ff05-4afb-aefd-c53ff7c89b3d)
- Log Output
![Log Output](https://github.com/suim-park/Cloud-Mini-Project-1/assets/143478016/400b0707-c137-45c1-b0df-190e56655c2b)

(2) Integrate AWS X-Ray tracing with the AWS Lambda function.
- In the AWS Lambda function, go to the `Configuration` section, then navigate to the `Monitoring and Operation Tools`, and enable the X-Ray Tracing feature.
![Enable X-Ray Tracing](https://github.com/suim-park/Cloud-Mini-Project-1/assets/143478016/152fb31c-36e8-4b58-a945-af03542f592d)
- See the X-Ray Tracing results.
![Trace Details-1](https://github.com/suim-park/Cloud-Mini-Project-1/assets/143478016/0d263479-b1ea-4138-aac4-ff2e6a8d0c42)
![Timeline](https://github.com/suim-park/Cloud-Mini-Project-1/assets/143478016/962691a8-4d21-4e83-9ebc-7b9028a7649a)

3. __`Connect logs/traces to CloudWatch`__: Watch the logs and traces in CloudWatch.
![Log Result](https://github.com/suim-park/Cloud-Mini-Project-1/assets/143478016/b443ea52-70ab-43e5-9487-bc65062d8a5d)