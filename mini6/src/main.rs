use lambda_runtime::{Error, LambdaEvent, service_fn};
use log::{info, warn};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use simple_logger::SimpleLogger;

#[derive(Deserialize)]
struct CustomEvent {
    numbers: Vec<i32>,
}

#[derive(Serialize)]
struct CustomOutput {
    average: Option<f64>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    SimpleLogger::new().init().unwrap();
    let func = service_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn func(event: LambdaEvent<Value>) -> Result<CustomOutput, Error> {
    let (event, _context) = event.into_parts();
    // Deserialize the event to our CustomEvent struct
    let event: CustomEvent = serde_json::from_value(event)?;

    match calculate_average(&event.numbers) {
        Some(average) => {
            info!("The average is: {}", average);
            Ok(CustomOutput { average: Some(average) })
        },
        None => {
            warn!("Could not calculate the average. The input list was empty.");
            Ok(CustomOutput { average: None })
        },
    }
}

/// Calculates the average of a list of numbers.
/// Logs the start and end of the computation, and warns if the list is empty.
fn calculate_average(numbers: &[i32]) -> Option<f64> {
    if numbers.is_empty() {
        warn!("Attempted to calculate the average of an empty list.");
        return None;
    }

    info!("Starting calculation for the average.");
    let sum: i32 = numbers.iter().sum();
    let count = numbers.len() as f64;
    let average = sum as f64 / count;

    info!("Finished calculation for the average.");
    Some(average)
}
